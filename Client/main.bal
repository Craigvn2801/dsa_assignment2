import ballerinax/kafka;
import ballerina/io;
import ballerina/lang.value;

#NB Create and edit topics
string DEFAULT_URL = "localhost: 9092";
string [] topics = ["writeFile", "requestFileWithKey"];

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);
kafka:ConsumerConfiguration consumerConfiguration = {
    groupId: "group-id",
    offsetReset: "earliest",
    topics: ["readFileResponse"]

};
kafka:Consumer consumer = check new (kafka:DEFAULT_URL, consumerConfiguration);

public function main() {

    io:println("***File Storage System***");
    io:println("1. Write file");
    io:println("2. Read file");
    io:println("\n");
    string option = io:readln("Select an option");

    match option{
        "1"=>{
            error? w = writeFile();
        }
        "2"=>{
            error? r = readFile();

        }
    }
}
public function writeFile()returns error?{
       string message = "Write into JSON";
       string fileKey = io:readln("Enter file key: ");
       string title = io:readln("Enter file name: ");
       string subject = io:readln("Enter subject: ");
       string fType = io:readln("Enter file type: ");
       string rating = io:readln("Enter rating: ");

       
       json fileObject = {fileKey, title, subject, fType, rating};
       io:println(fileObject);

       check kafkaProducer->send({
                               topic: topics[0],
                               value: fileObject.toJsonString().toBytes() });

       check kafkaProducer->'flush();
       io:println("\n");
       main();
    }

public function readFile()returns error?{
       string fileKey = io:readln("Enter file name: ");

       check kafkaProducer->send({
                                    topic: topics[1],
                                    value: fileKey.toBytes()
                                    });
        check kafkaProducer->'flush();

kafka:ConsumerRecord[] records = check consumer->poll(1);
int i=0;
foreach var kafkaRecord in records {
    byte[] messageContent = kafkaRecord.value;
    string message = check string:fromBytes(messageContent);
    io:println("Kafka ",i+1);

    json fileObject = check value:fromJsonString(message);
    io:println("\n\n\n");
    if(fileObject.fileKey == fileKey){
        io:println("Object recieved: ",fileObject);

        break;
    }
}
       main();
}
