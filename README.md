# DSA_Assignment2

Members:
* Keith Craig van Niekerk - 219002444
* Justin Brandt - 220040869

Problem

Distributed Storage System.
Client multiple instances of a server.
Adopt asynchronous peer to peer replication among server instances with replication factor of three.
Supports 2 operations: Read and Write objects.
Each object is in JSON format and stored in a local file for each instance of the server. Size limit 30mb.
Each object has a key(String) attached to it.
Kafka middleware between client and server(s).

To write an object, the JSON content and the key are sent to the server through kafka.
When write operation succeeds client recieves an acknowledgement from each server.
A write operation with an existing key replaces the existing object.
Client sends a request through Kafka and recieves a copy from each server with the logical clock attached to it to read an object.
Client analyses the logical clocks decides upon the latest version and prints it out.
Multiple concurrent versions, it prints them all out.
Client identife=ies that a given copy has a version older thn the other version, it notifies the replica, whcih then updates its store. 
Replicas keep a direct connection to each other and exchange a heartbeat message regularly with their logical piggybacked to the message.
Adopt vector clocks as a logical clock.

Implement using Kafka cluster

Criteria
Setup Kafka cluster, including topic management.(20%)
Implementation of the client and servies in the Ballerina language. (70%)
Quality of design and implementation. (10%)
